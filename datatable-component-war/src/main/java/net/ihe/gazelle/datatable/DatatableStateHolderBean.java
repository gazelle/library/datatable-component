/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package net.ihe.gazelle.datatable;

import org.richfaces.component.SortOrder;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.HashMap;
import java.util.Map;

@ManagedBean(name = "dataTableStateHolder")
@ViewScoped
public class DatatableStateHolderBean implements DatatableStateHolder {

	private static final long serialVersionUID = 1869072214666544359L;

	private Map<String, SortOrder> sortOrders = new HashMap<String, SortOrder>();

	private Map<String, Object> columnFilterValues = new HashMap<String, Object>();

	@Override
	public Map<String, Object> getColumnFilterValues() {
		return columnFilterValues;
	}

	@Override
	public Map<String, SortOrder> getSortOrders() {
		return sortOrders;
	}

	@Override
	public void setAscendingOn(String column) {
		sortOrders.put(column, SortOrder.ascending);
	}

	@Override
	public void setDescendingOn(String column) {
		sortOrders.put(column, SortOrder.descending);
	}

}
