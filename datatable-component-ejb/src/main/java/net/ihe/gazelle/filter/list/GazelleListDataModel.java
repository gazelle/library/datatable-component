package net.ihe.gazelle.filter.list;

import org.ajax4jsf.model.*;
import org.richfaces.component.SortOrder;
import org.richfaces.model.Arrangeable;
import org.richfaces.model.ArrangeableState;
import org.richfaces.model.FilterField;
import org.richfaces.model.SortField;

import javax.el.ELException;
import javax.el.Expression;
import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.List;

public class GazelleListDataModel<T> extends ExtendedDataModel<T>
        implements Arrangeable, Serializable {

    private static final long serialVersionUID = 8773031927238386576L;

    private List<T> listElements;

    private Object dataItem;

    private SequenceRange cachedRange;

    private transient WeakReference<List<T>> weakCache;

    private ArrangeableState arrangeableState;

    public GazelleListDataModel(List<T> listElements) {
        super();
        this.listElements = listElements;
    }

    public List<T> getListElements() {
        return listElements;
    }

    public void setListElements(List<T> listElements) {
        this.listElements = listElements;
    }

	public String getIdPropertyName() {
		return "_id";
	}

    private static boolean areEqualRanges(SequenceRange range1,
                                          SequenceRange range2) {
        if ((range1 == null) || (range2 == null)) {
            return (range1 == null) && (range2 == null);
        } else {
            return (range1.getFirstRow() == range2.getFirstRow())
                    && (range1.getRows() == range2.getRows());
        }
    }

    private String getPropertyName(FacesContext facesContext,
                                   Expression expression) {
        try {
            return (String) ((ValueExpression) expression)
                    .getValue(facesContext.getELContext());
        } catch (ELException e) {
            throw new FacesException(e.getMessage(), e);
        }
    }

    @Override
    public Object getRowKey() {
        return dataItem;
    }

    @Override
    public void setRowKey(Object key) {
        this.dataItem = key;
    }

    public List<T> getAllItems(FacesContext facesContext) {
        walk(facesContext, new DataVisitor() {
            @Override
            public DataVisitResult process(FacesContext facescontext,
                                           Object obj, Object obj1) {
                return DataVisitResult.CONTINUE;
            }
        }, null, null);
        return getWeakCache().get();
    }

    protected void appendFilters(FacesContext context,
                                 ListQueryBuilder<T> queryBuilder) {
        //
    }

    protected void appendUserFilters(FacesContext context,
                                     ListQueryBuilder<T> queryBuilder) {
        if (arrangeableState != null && (arrangeableState.getFilterFields() != null)
                && (context != null)) {
            for (FilterField filterField : arrangeableState
                    .getFilterFields()) {
                String filterValue = (String) filterField.getFilterValue();
                if ((filterValue != null) && (filterValue.length() != 0)) {
                    String propertyName = getPropertyName(context,
                            filterField.getFilterExpression());
                    if (propertyName != null) {
                        queryBuilder.addLike(propertyName, filterValue);
                    } else {
                        System.err.println(filterField
                                .getFilterExpression()
                                + " is not a valid value");
                    }
                }
            }
        }
    }

    protected void appendSorts(FacesContext context,
                               ListQueryBuilder<T> queryBuilder) {
        if (arrangeableState != null && (arrangeableState.getSortFields() != null) && (context != null)) {
            for (SortField sortField : arrangeableState.getSortFields()) {
                SortOrder ordering = sortField.getSortOrder();

                if (SortOrder.ascending.equals(ordering)
                        || SortOrder.descending.equals(ordering)) {
                    String propertyName = getPropertyName(context,
                            sortField.getSortBy());
                    if (propertyName != null) {
                        queryBuilder.addOrder(propertyName,
                                SortOrder.ascending.equals(ordering));
                    } else {
                        System.err.println(sortField.getSortBy()
                                + " is not a valid sort");
                    }
                }
            }
        }
    }

    @Override
    public void walk(FacesContext facesContext, DataVisitor visitor,
                     Range range, Object argument) {

        SequenceRange jpaRange = (SequenceRange) range;

        List<T> result = getWeakCache().get();
        if ((result == null) || !areEqualRanges(this.cachedRange, jpaRange)) {

            ListQueryBuilder<T> queryBuilder = prepareQueryBuilder(facesContext, true);

            if (jpaRange != null) {
                int first = jpaRange.getFirstRow();
                int rows = jpaRange.getRows();

                queryBuilder.setFirstResult(first);
                if (rows > 0) {
                    queryBuilder.setMaxResults(rows);
                }
            }

            this.cachedRange = jpaRange;
            result = (List<T>) getRealCache(queryBuilder);
            weakCache = new WeakReference<List<T>>(result);
        }

        for (T t : result) {
            visitor.process(facesContext, getId(t), argument);
        }
    }

    public ListQueryBuilder<T> prepareQueryBuilder(FacesContext facesContext,
                                                   boolean appendSorts) {
        ListQueryBuilder<T> queryBuilder;
        queryBuilder = new ListQueryBuilder<T>(listElements);
        appendFilters(facesContext, queryBuilder);
        appendUserFilters(facesContext, queryBuilder);
        if (appendSorts) {
            appendSorts(facesContext, queryBuilder);
        }
        return queryBuilder;
    }

    protected int getRealCount(ListQueryBuilder<T> queryBuilder) {
        return queryBuilder.getCount();
    }

    protected List<?> getRealCache(ListQueryBuilder<T> queryBuilder) {
        return queryBuilder.getList();
    }

    /**
     * For list compatibility
     *
     * @return row count
     */
    public int size() {
        return getRowCount();
    }

    @Override
    public int getRowCount() {
        ListQueryBuilder<T> queryBuilder = prepareQueryBuilder(FacesContext.getCurrentInstance(), false);
        return getRealCount(queryBuilder);
    }

    @Override
    public T getRowData() {
        ListQueryBuilder<T> queryBuilder = new ListQueryBuilder<T>(listElements);
        queryBuilder.addEq(this.getIdPropertyName(), this.dataItem);
        return queryBuilder.getUniqueResult();
    }

    @Override
    public int getRowIndex() {
        return (this.dataItem != null) ? (getAsInteger(this.getDataItem())) : -1;
    }

    private Integer getAsInteger(Object dataItem2) {
        try {
            return Integer.valueOf(dataItem2.toString());
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public Object getWrappedData() {
        return getWeakCache().get();
    }

    @Override
    public boolean isRowAvailable() {
        return (this.dataItem != null);
    }

    @Override
    public void setRowIndex(int rowIndex) {
    }

    @Override
    public void setWrappedData(Object data) {
    }

    public void resetCache() {
        this.weakCache = new WeakReference<List<T>>(null);
        this.cachedRange = null;
    }

    public WeakReference<List<T>> getWeakCache() {
        if (weakCache == null) {
            weakCache = new WeakReference<List<T>>(null);
        }
        return weakCache;
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        in.defaultReadObject();
        resetCache();
    }

    @Override
    public void arrange(FacesContext context, ArrangeableState state) {
        arrangeableState = state;
        resetCache();
    }

//	protected abstract Object getId(T t);

    protected Object getId(T t) {
        if (this.listElements != null) {
            int i = 0;
            for (T el : this.listElements) {
                if (el == t) {
                    return i;
                }
                i++;
            }
        }
        return null;
    }

    public Object getDataItem() {
        return dataItem;
    }

    public void setDataItem(Object dataItem) {
        this.dataItem = dataItem;
    }

    public SequenceRange getCachedRange() {
        return cachedRange;
    }

    public void setCachedRange(SequenceRange cachedRange) {
        this.cachedRange = cachedRange;
    }

    public ArrangeableState getArrangeableState() {
        return arrangeableState;
    }

    public void setArrangeableState(ArrangeableState arrangeableState) {
        this.arrangeableState = arrangeableState;
    }

    public void setWeakCache(WeakReference<List<T>> weakCache) {
        this.weakCache = weakCache;
    }
}
