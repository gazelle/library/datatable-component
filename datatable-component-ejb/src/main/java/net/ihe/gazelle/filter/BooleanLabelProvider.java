package net.ihe.gazelle.filter;

import net.ihe.gazelle.hql.LabelProvider;
import net.ihe.gazelle.ui.services.TranslateProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.spi.CDI;
import javax.faces.context.FacesContext;
import java.util.ResourceBundle;

public class BooleanLabelProvider implements LabelProvider<Boolean> {

    private static final Logger log = LoggerFactory.getLogger(BooleanLabelProvider.class);

    private BooleanLabelProvider() {
        super();
    }

    @Override
    public String getLabel(Boolean instance) {
        if (log.isDebugEnabled()) {
            log.debug("getLabel");
        }
        if (instance == null) {
            return "?";
        } else {
            if (instance) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                String label = null;
                try{
                    label = CDI.current().select(TranslateProvider.class).get().getTranslation("net.ihe.gazelle.True");
                }catch (Exception e){
                    log.error("Error looking for translation with key : {}", "net.ihe.gazelle.True");
                    log.error("", e);
                }
                return label == null ? "Yes" : label;
            } else {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                String label = null;
                try{
                    label = CDI.current().select(TranslateProvider.class).get().getTranslation("net.ihe.gazelle.False");
                }catch (Exception e){
                    log.error("Error looking for translation with key : {}", "net.ihe.gazelle.False");
                    log.error("", e);
                }
                return label == null ? "No" : label;
            }
        }
    }

}
