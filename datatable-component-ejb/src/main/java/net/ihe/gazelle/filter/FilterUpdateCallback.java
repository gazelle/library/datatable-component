package net.ihe.gazelle.filter;

public interface FilterUpdateCallback {

    void filterModified();
}
