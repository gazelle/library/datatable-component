package net.ihe.gazelle.datatable;

import org.richfaces.component.SortOrder;

import java.io.Serializable;
import java.util.Map;

public interface DatatableStateHolder extends Serializable {
    Map<String, Object> getColumnFilterValues();

    Map<String, SortOrder> getSortOrders();

    void setAscendingOn(String column);

    void setDescendingOn(String column);
}
